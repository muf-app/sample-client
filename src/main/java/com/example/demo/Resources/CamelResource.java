package com.example.demo.Resources;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/camel")
public class CamelResource {
    @GetMapping("example")
    public ResponseEntity<String> getExample(){
        return new ResponseEntity<String>(null, null, HttpStatus.OK);
    }
}
