package com.example.demo.Resources;

import com.google.gson.Gson;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.net.ConnectException;

@RestController
@RequestMapping("/api")
public class HttpResource {
    private final String rightIP = "http://xxx";
    private final String wrongIP = "http://10.22.15.192";

    private String getResponseFromLos(String ipAddress) throws Exception {
        String result = "";

        String obj = new JSONObject()
                .put("idApplication", "01041900001064")
                .put("productName", "MOTOR BARU")
                .put("idGood", "16123")
                .toString();

        Gson gson = new Gson();
        String jsonified = gson.toJson(obj);

        String url = ipAddress + "/los/api/internal/unit?data={jsonified}";

        //TODO: start how to handle http request
        try{
            RestTemplate restTemplate = new RestTemplate();
            result = restTemplate.getForObject(url, String.class, jsonified);
        }catch (HttpStatusCodeException e){ // exception dari http
            // TODO: berikut ini cara untuk capture content via header
            throw new Exception(e.getResponseHeaders().get("error_message").get(0));
        }catch (Exception e){ // layer terendah dari exception, untuk menangkap semua exception
            // TODO: Berikut ini cara untuk capture untuk response lain nya
            throw new Exception(e.getMessage());
        }
        //TODO: end how to handle http request

        return result;
    }

    @GetMapping("/handleerror")
    public ResponseEntity<String> handleError(){
        String a;
        try {
            a = this.getResponseFromLos(this.rightIP);
            return new ResponseEntity<String>(a, null, HttpStatus.OK);
        }catch (Exception e){
            a = e.getMessage();

            HttpHeaders headers = new HttpHeaders();
            headers.add("message", a);
            return new ResponseEntity<String>(null, headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
